import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
// import path from 'path';
import seedRouter from './routes/seedRoutes.js';
import productRouter from './routes/productRoutes.js';
import userRouter from './routes/userRoutes.js';
import orderRouter from './routes/orderRoutes.js';
import uploadRouter from './routes/uploadRoutes.js';

dotenv.config();

const app = express();

mongoose
  .connect(process.env.MONGO_URI)
  .then(() => {
    console.log('connected to cloud database');
  })
  .catch((err) => {
    console.log(err.message);
  });

// const corsOrigin = {
//   headers: {
//     'Access-Control-Allow-Origin': '*',
//     'Access-Control-Allow-Methods': 'GET, PUT, POST, DELTE, PATCH, OPTIONS',
//     'Access-Control-Allow-Headers':
//       'Content-Type, X-Auth-Token, Origin, Authorization',
//   },
//   credentitals: true,
//   optionSuccessStatus: 200,
// };
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/api/keys/paypal', cors(), (req, res) => {
  res.send(process.env.PAYPAL_CLIENT_ID || 'sb');
});

app.use('/api/upload', uploadRouter);
app.use('/api/seed', seedRouter);
app.use('/api/products', productRouter);
app.use('/api/users', userRouter);
app.use('/api/orders', orderRouter);

// const __dirname = path.resolve();
// app.use(express.static(path.join(__dirname, '/frontend/build')));
// app.get('*', (req, res) =>
//   res.sendFile(path.join(__dirname, '/frontend/build/index.html')),
// );

app.use((err, req, res, next) => {
  res.status(500).send({ message: err.message });
});

const port = 4000;
app.listen(port, () => {
  console.log(`server is live on port ${process.env.PORT} || ${port}`);
});
